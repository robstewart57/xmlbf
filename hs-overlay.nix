{ pkgs }:

# To be used as `packageSetConfig` for a Haskell pacakge set:
self: super:
{
  html-entities = pkgs.haskell.lib.overrideCabal super.html-entities (_: {
    postUnpack = "rm html-entities-1.1.4.2/Setup.hs";
  });
  xmlbf = super.callPackage ./xmlbf/pkg.nix {};
  xmlbf-xeno = super.callPackage ./xmlbf-xeno/pkg.nix { inherit (self) xmlbf; };
  xmlbf-xmlhtml = super.callPackage ./xmlbf-xmlhtml/pkg.nix { inherit (self) xmlbf; };
}
