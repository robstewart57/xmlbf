# 0.1.2

* Made compatible with `xmlbf-0.5`.


# 0.1.1

* Made compatible with `xmlbf-0.3`.


# 0.1

* First version.
